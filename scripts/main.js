var app = angular.module('Routing', [
  'ngRoute'
]);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when("/", {templateUrl: "pages/home.html", controller: "HomeController"})
    .when("/users", {templateUrl: "pages/users.html", controller: "HomeController"})
    .when("/user/get", {templateUrl: "pages/getUser.html", controller: "HomeController"})
    .when("/user/post", {templateUrl: "pages/createUser.html", controller: "HomeController"})
    .when("/user/delete", {templateUrl: "pages/deleteUser.html", controller: "HomeController"})
    .otherwise("/404", {templateUrl: "pages/404.html", controller: "HomeController"});
}]);

app.controller('HomeController', function($scope, $location, $http) {
  console.log("test home controller");

  $scope.selectedPerson = null;

  $scope.users = [
    {
      username: "salihgungor",
      name: "Salih",
      surname: "Güngör",
      email: "salihgungor13@hotmail.com",
      age: 23,
      gender: "male",
      department: "IT"
    }, {
      username: "emirsacit",
      name: "Sacit",
      surname: "Güngör",
      email: "sacitgungor@hotmail.com",
      age: 5,
      gender: "male",
      department: "Network"
    }
  ];

  $scope.getUser = () => {
    $scope.errorText = null;
    $scope.selectedPerson = null;
   
    if (!$scope.username) return;
    $scope.users.forEach(element => {
      if (element.username === $scope.username) $scope.selectedPerson = element;
    });

    if ($scope.selectedPerson === null) $scope.errorText = "User not found.";

  }
});

